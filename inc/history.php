<?php

if(!function_exists(THEME_PREFIX.'_history_item_init'))
{
  function epice_history_item_init()
  {
    $labels = array(
      'name'          => __('History blocks',THEME_PREFIX),
      'singular_name' => __('History block',THEME_PREFIX),
      'add_new'       => __('Add history block',THEME_PREFIX),
      'add_new_item'  => __('Add new history block',THEME_PREFIX),
      'menu_name'     => __('History blocks',THEME_PREFIX)
    );
    $args = array(
      'labels' => $labels,
      'public' => true,
      'has_archive' => true,
      //'rewrite' => array('slug' => 'history'),
      //'taxonomies' => array(THEME_PREFIX.'_program_type',THEME_PREFIX.'_program_universe'),
      //'taxonomies' => array('category'),
      'supports' => array('title')
    );
    register_post_type(THEME_PREFIX.'_history', $args);
  }
}
add_action('init',THEME_PREFIX.'_history_item_init');

$historyMetaBox = array(
  'id' => THEME_PREFIX.'_history_metabox',
  'title' => __('History block',THEME_PREFIX),
  'page' => THEME_PREFIX.'_history',
  'context' => 'normal',
  'priority' => 'high',
  'fields' => array(
    array(
      'name' => THEME_PREFIX.'HistoryDate',
      'desc' => __('History Date',THEME_PREFIX),
      'id' => THEME_PREFIX.'HistoryDate',
      'class' => THEME_PREFIX.'_history_date',
      'type' => 'text'
    ),
    array(
      'name' => THEME_PREFIX.'HistoryDescription',
      'desc' => __('History Description',THEME_PREFIX),
      'id' => THEME_PREFIX.'HistoryDescription',
      'class' => THEME_PREFIX.'_history_description',
      'type' => 'block-text'
    ),
    array(
      'name' => THEME_PREFIX.'HistoryPicture',
      'desc' => __('History picture',THEME_PREFIX),
      'id' => THEME_PREFIX.'HistoryPicture',
      'class' => 'image_upload_button',
      'type' => 'media'
    )
  )
);

if(!function_exists(THEME_PREFIX.'_history_metabox'))
{
  function epice_history_metabox($post,$metabox)
  {
    global $historyMetaBox;
    global $wpdb;

    $output = '<input type="hidden" name="history_noncename" id="history_noncename" value="'.wp_create_nonce($post->ID).'" />';

    foreach($historyMetaBox['fields'] as $mainField)
    {
      $output .= createAdminCode($mainField,$post,$wpdb);
    }

    /*foreach($historyMetaBox['groups'] as $group)
    {
      $output .= '<div class="post-content '.$group['type'].'">';
      $output .= '<h4>'.$group['title'].'</h4>';
      
      foreach($group['fields'] as $field)
      {
        $output .= createAdminCode($field,$post,$wpdb);
      }

      $output .= '</div>';
    }*/

    echo $output;
  }
}
if(!function_exists('add_'.THEME_PREFIX.'_history_metaboxes'))
{
  function add_epice_history_metaboxes()
  {
    global $historyMetaBox;
    add_meta_box($historyMetaBox['id'],$historyMetaBox['title'],'epice_history_metabox',$historyMetaBox['page'],$historyMetaBox['context'],$historyMetaBox['priority']);
  }
}
add_action('add_meta_boxes','add_'.THEME_PREFIX.'_history_metaboxes');















if(!function_exists('save_'.THEME_PREFIX.'_history'))
{
  function save_epice_history($post_id,$post)
  {
    global $historyMetaBox;
// || defined('DOING_AJAX' ) || (array_key_exists('program_noncename',$_POST) && !wp_verify_nonce($_POST['program_noncename'],$post_id))
    if(empty($_POST) || !current_user_can('edit_post',$post_id) || $post->post_type == 'revision' || $post->post_type != THEME_PREFIX.'_history' || (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) || (!wp_verify_nonce($_POST['history_noncename'],$post_id) && !check_ajax_referer('inlineeditnonce','_inline_edit')))
    {
      return $post_id;
    }
    
// Tester le type pour n'enregistrer que les données correspondantes ??? utile ???

    foreach($historyMetaBox['fields'] as $mainField)
    {
      saveField($mainField,$post_id);
    }

    /*foreach($programMetaBox['groups'] as $group)
    {
      foreach($group['fields'] as $field)
      {
        saveField($field,$post_id);
      }
    }*/
  }
}
add_action('save_post','save_'.THEME_PREFIX.'_history',1,2);






if(!function_exists('remove_media_buttons'))
{
  function remove_media_buttons()
  {
    global $post;
//($post->post_type == THEME_PREFIX.'_history' || $post->post_type == THEME_PREFIX.'_program')
    if($post->post_type == THEME_PREFIX.'_history' && current_user_can('edit_post'))
    {
      remove_action('media_buttons','media_buttons');
    }
  }
}
//add_action('admin_head','remove_media_buttons');

if(!function_exists('embedUploaderCode'))
{
  function embedUploaderCode()
  {
    add_thickbox();
    wp_enqueue_script(THEME_PREFIX.'-admin-lib',THEME_URI.'/js/epice-admin.js',array(),'1.0',true);
  }
}
add_action('admin_enqueue_scripts','embedUploaderCode');




if(!function_exists(THEME_PREFIX.'_history_collection'))
{
  function epice_history_collection($atts)
  {
    /*$defaultLocale = 'fr_FR';
    $locale = get_locale();

    if(empty($locale))
    {
      $locale = $defaultLocale;
    }*/
    
    $atts = shortcode_atts(array(
      'posts_per_page' => 10,
      'order' => 'ASC'
    ),$atts,'history_collection');

    $argsHistory = array(
      'post_type' => THEME_PREFIX.'_history',
      'posts_per_page' => $atts['posts_per_page'],
      'meta_key'   => THEME_PREFIX.'HistoryDate',
      'orderby'    => 'meta_value_num',
      'order'      => $atts['order']
    );
    $loopHistory = new WP_Query($argsHistory);

    $histCount = 0;

    if($loopHistory->have_posts()): ?>
    <ol reversed="reversed" class="history-list">
    <?php while($loopHistory->have_posts()): $loopHistory->the_post(); ?>
    <?php set_query_var('index',$histCount);get_template_part('parts/history-block'); ?>
    <?php $histCount++; endwhile; ?>
    </ol>
    <?php endif; wp_reset_query();
  }
}
add_shortcode('history_collection',THEME_PREFIX.'_history_collection');
