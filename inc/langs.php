<?php
/*
add_filter('pll_get_post_types','add_cpt_to_pll',10,2);
function add_cpt_to_pll($post_types, $hide) {
  if ($hide)
    // hides 'my_cpt' from the list of custom post types in Polylang settings
    unset($post_types[THEME_PREFIX.'_history']);
  else
    // enables language and translation management for 'my_cpt'
    $post_types[THEME_PREFIX.'_history'] = THEME_PREFIX.'_history';
  return $post_types;
}
*/

// Code de locale correct pour le japonais
if(!function_exists(THEME_PREFIX.'_site_language'))
{
  function epice_site_language($bi)
  {
    if($bi == 'ja')
    {
      $bi = 'jp-JP';
    }

    return $bi;
  }
}
add_filter('bloginfo',THEME_PREFIX.'_site_language');
