<?php

function createAdminCode($field,$post,$wpdb)
{
  $meta = get_post_meta($post->ID,$field['id'], true);

  $fieldTitle = 'h5';

  $output = '<div';

  if($field['type'] == 'media')
  {
    $output .= ' class="media-box">';
    $output .= '<'.$fieldTitle.'>'.$field['desc'].'</'.$fieldTitle.'>';

    if($meta)
    {
      $id = $wpdb->get_var('SELECT ID FROM '.$wpdb->posts.' WHERE guid = "'.$meta.'" AND post_type="attachment" LIMIT 1');
      $output .= '<p class="'.THEME_PREFIX.'-preview"><a target="_blank" href="'.$meta.'">'.wp_get_attachment_image($id,'full').'</a></p>';
    }
    else
    {
      $output .= '<p class="'.THEME_PREFIX.'-preview">'.__('No picture',THEME_PREFIX).'</p>';
    }

    $output .= '<input type="hidden" class="metaValueField" id="'.$field['id'].'" name="'.$field['id'].'" value="'.$meta.'" />
        <input type="button" value="'.__('Choose file',THEME_PREFIX).'" class="'.$field['class'].' button button-small" />
        <input class="removeImageBtn button button-small" type="button" value="'.__('Remove File',THEME_PREFIX).'" />';
  }
  else if($field['type'] == 'select')
  {
    $output .= '>';
    $output .= '<'.$fieldTitle.'><label for="'.$field['id'].'">'.$field['desc'].'</label></'.$fieldTitle.'>
    <select id="'.$field['id'].'" name="'.$field['id'].'">';

    foreach($field['values'] as $key => $value)
    {
      $selectedCode = '';

      if($meta == $key)
      {
        $selectedCode = ' selected="selected"';
      }
      
      $output .= '<option value="'.$key.'"'.$selectedCode.'>'.$value['title'].'</option>';
    }

    $output .= '</select>';
  }
  else if($field['type'] == 'block-text')
  {
    $output .= '>';
    $output .= '<'.$fieldTitle.'><label for="'.$field['id'].'">'.$field['desc'].'</label></'.$fieldTitle.'>
    <textarea name="'.$field['name'].'" id="'.$field['id'].'" class="'.$field['class'].'">'.$meta.'</textarea>';
  }
  else
  {
    $output .= '>';
    $output .= '<'.$fieldTitle.'><label for="'.$field['id'].'">'.$field['desc'].'</label></'.$fieldTitle.'>
    <input type="text" name="'.$field['name'].'" id="'.$field['id'].'" value="'.$meta.'" class="'.$field['class'].'" />';
  }

  $output .= '</div>';

  return $output;
}

function saveField($field,$post_id)
{
  $key = $field['id'];
  if(array_key_exists($key,$_POST))
  {
    $value = sanitize_text_field($_POST[$key]);

    if(get_post_meta($post_id,$key))
    {
      if(!$value)
      {
        delete_post_meta($post_id,$key);
      }
      else
      {
        update_post_meta($post_id,$key,$value);
      }
    }
    else
    {
      add_post_meta($post_id,$key,$value);
    }
  }
}
