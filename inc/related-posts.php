<?php

if(!function_exists(THEME_PREFIX.'_connection_types'))
{
  function epice_connection_types()
  {
    p2p_register_connection_type( array(
        'name' => 'posts_to_posts',
        'from' => 'post',
        'to' => 'post'
    ) );
  }
}
add_action( 'p2p_init', THEME_PREFIX.'_connection_types' );
