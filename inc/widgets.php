<?php

if(!function_exists(THEME_PREFIX.'_theme_setup'))
{
  function epice_widgets_init()
  {
    register_sidebar(array(
      'name'          => __('Header right sidebar'),
      'id'            => 'header_right_1',
      'before_widget' => '<div>',
      'after_widget'  => '</div>',
      'before_title'  => '<h2 class="widget-title">',
      'after_title'   => '</h2>',
    ));
  }
}
add_action('widgets_init',THEME_PREFIX.'_widgets_init' );
