<?php

/*
Template Name: Newsletter
*/

get_header(); ?>


<?php if(have_posts()): ?>

<?php while(have_posts()): the_post();?>
<h1><?php the_title(); ?></h1>

<div class="newsletter-page">
<p class="back"><a href="<?php echo wp_get_referer(); ?>"><?php _e('Back'); ?></a></p>
<?php get_template_part('parts/content-page'); ?>
</div>

<?php endwhile; ?>

<?php endif; ?>


<?php

get_footer();
