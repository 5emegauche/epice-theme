  </div>
  
<?php if(!is_front_page() && !is_category()): ?>
<p class="desktop-element"><a href="#contenu" class="image-text" id="back-to-top"><?php _e('Back to top');?></a></p>
<?php endif; ?>
  </main>



<?php if(is_single() && has_action('p2p_init',THEME_PREFIX.'_connection_types')): ?>

  <?php
  // Find connected pages
  $connected = new WP_Query(array(
    'connected_type' => 'posts_to_posts',
    'connected_items' => get_queried_object(),
    'nopaging' => true
  ));

  // Display connected pages
  if($connected->have_posts()):
  ?>
  <div id="related-posts">
  <h3><?php _e('Related posts:',THEME_PREFIX); ?></h3>
  <ul id="related-posts-list">
  <?php while ( $connected->have_posts() ) : $connected->the_post(); ?>
  <li class="picture-block-item">
  <?php if(has_post_thumbnail()): ?>
  <a href="<?php the_permalink(); ?>" class="block-link"><?php the_post_thumbnail('full'); ?></a>
  <?php endif; ?>
  <div class="block-content">
  <?php the_category(); ?>
  <h3><?php the_title(); ?></h3>
  </div>
  </li>
  <?php endwhile; ?>
  </ul>
  </div>
  <?php endif; wp_reset_postdata(); ?>

<?php endif; ?>

<?php /* ﾄﾊﾈﾍﾑゞ */ ?>

<?php get_template_part('parts/page-footer'); ?>
</div>


<?php wp_footer(); ?>
<?php /*
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-295523-47']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
*/ ?>
</body>
</html>
