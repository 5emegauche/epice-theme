<?php

define('THEME_PREFIX','epice');
define('THEME_URI',get_template_directory_uri());
define('THEME_PATH',get_template_directory());



require_once('inc/core.php');

require_once('inc/langs.php');
require_once('inc/related-posts.php');
require_once('inc/widgets.php');
require_once('inc/history.php');


// Zones de menu du thème

register_nav_menu('catheader',__('Menu category header',THEME_PREFIX));
register_nav_menu('leftheader',__('Menu navigation header left',THEME_PREFIX));
register_nav_menu('rightheader',__('Menu navigation header right',THEME_PREFIX));
register_nav_menu('footer',__('Menu navigation footer',THEME_PREFIX));

if(!function_exists(THEME_PREFIX.'_scripts_styles'))
{
  function epice_scripts_styles()
  {
    global $wp_styles;
    //wp_deregister_script('jquery');

    //wp_enqueue_style(THEME_PREFIX.'font-style','http://fonts.googleapis.com/css?family=Ubuntu',array(),'1.0');

    $locale = get_locale();
    if($locale == 'ja')
    {
      wp_enqueue_style(THEME_PREFIX.'ja-font','http://fonts.googleapis.com/earlyaccess/notosansjapanese.css',array(),'1.0');
    }
    
    wp_enqueue_style(THEME_PREFIX.'-style',get_stylesheet_uri(),array(),'1.0');

    wp_enqueue_style('ie_css',get_template_directory_uri().'/css/ie.css',array(),'1.0');
    $wp_styles->add_data('ie_css','conditional','IE');

    wp_enqueue_script(THEME_PREFIX.'-core',THEME_URI.'/js/epice-core.js',array('jquery'),'1.0',true);

    wp_register_script(THEME_PREFIX.'-ajax',THEME_URI.'/js/epice-ajax.js',array('jquery'),'1.0',true);
    wp_localize_script(THEME_PREFIX.'-ajax','homePosts',array('ajaxurl' => admin_url('admin-ajax.php')));
    wp_enqueue_script(THEME_PREFIX.'-ajax');
  }
}
add_action('wp_enqueue_scripts',THEME_PREFIX.'_scripts_styles');


if(!function_exists(THEME_PREFIX.'_scripts_config'))
{
  function epice_scripts_config()
  {
    ?>
<script type="text/javascript">
/* <![CDATA[ */

var pageConfig = {
  'themeUrl': '<?php echo THEME_URI; ?>'
};

/* ]]> */
</script>
    <?php
  }
}
add_action('wp_head',THEME_PREFIX.'_scripts_config');

if(!function_exists(THEME_PREFIX.'_theme_setup'))
{
  function epice_theme_setup()
  {
    /*add_theme_support('custom-header',$args);
    add_theme_support('post-formats',array('aside','gallery','image'));*/
    add_theme_support('post-thumbnails');
    set_post_thumbnail_size(370,370);
    set_post_thumbnail_size(370,505);
    add_theme_support('title-tag');
    add_theme_support('html5', array('comment-list','comment-form','search-form','gallery','caption'));

    load_theme_textdomain(THEME_PREFIX,THEME_PATH.'/languages');

    remove_action('init', 'origin_widgets_display_css');
  }
}
add_action('after_setup_theme','epice_theme_setup');

if(!function_exists(THEME_PREFIX.'_style_admin'))
{
  function epice_style_admin()
  {
    wp_enqueue_style(THEME_PREFIX.'-admin-style',THEME_URI.'/css/admin.css',array(),'1.0');
  }
}
add_action( 'admin_init', THEME_PREFIX.'_style_admin' );


// Ajout de la locale à la classe du body

if(!function_exists(THEME_PREFIX.'_body_classes'))
{
  function epice_body_classes($classes)
  {
    //$id = get_current_blog_id();
    $locale = get_locale();
    //$classes[] = 'site-id-'.$id;
    $classes[] = $locale;
    return $classes;
  }
}
add_filter('body_class',THEME_PREFIX.'_body_classes');


function epice_filter_posts()
{
  $posts = array();

  $cat = $_GET['category'];

  $argsPost = array(
    'post_type' => 'post'
  );

  if($cat == '')
  {
    
  }
  else
  {
    $argsPost['cat'] = $cat;
  }

  $loopPost = new WP_Query($argsPost);
  while($loopPost->have_posts())
  {
    $loopPost->the_post();

    $blockCats = get_the_category();
    $blockCat = $blockCats[0];

    $post_thumbnail_id = get_post_thumbnail_id();
    $image = wp_get_attachment_image_src($post_thumbnail_id,'full');

    $url = get_the_permalink();

    $item = array(
      'title' => get_the_title(),
      'category' => $blockCat->cat_name,
      'categoryId' => $argsPost['cat'],
      'image' => $image[0],
      'url' => $url
    );

    $posts[] = $item;
  }
  
  $result = json_encode($posts);
  header('Content-type: application/json');
  echo $result;
  die();
}

add_action('wp_ajax_nopriv_filter_posts',THEME_PREFIX.'_filter_posts');


// Styles du language switcher

if(!function_exists(THEME_PREFIX.'_msls_output_get'))
{
  function epice_msls_output_get($url,$link,$current)
  {
    $linkCode = '';

    if($current)
    {
      $linkCode = sprintf(
        '<span title="%s" class="current">%s</span>',
        $link->txt,
        $link
      );
    }
    else
    {
      $linkCode = sprintf(
        '<a href="%s" title="%s">%s</a>',
        $url,
        $link->txt,
        $link
      );
    }

    return $linkCode;
  }
}
add_filter( 'msls_output_get',THEME_PREFIX.'_msls_output_get',10,3);

/*
function  processMyForm()
{
 
    // Vous pouvez faire une certaine validation même si la validation
    // Est également traité dans notre fonction
  $email = $_POST['email'];
  $list = $_POST['list'];
  
  // Dans ce prénom et nom de tableau sont facultatifs
  $user_data = array(
    'email' => $email
  );

  $data_subscriber  = array(
    'user' => $user_data,
    'user_list' => array('list_ids' => array($list))
  );

  $helper_user = WYSIJA::get('user','helper');
  
  $helper_user->addSubscriber($data_subscriber);
    
 
    // Si la double optin est sur elle enverra un email de confirmation
    // Pour l'abonné
 
    // Si la double optin est éteint et que vous avez une automatique actif
    // Bulletin alors il enverra la lettre d'information automatique pour l'abonné
}
 
// Initialisation de cette fonction que lorsque les données de votre formulaire d'inscription a été posté
add_action ('init','processMyForm');
*/
