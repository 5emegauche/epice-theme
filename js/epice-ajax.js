if(window.jQuery !== undefined)
{
  var jqAjax = jQuery.noConflict();

  var _epiceAConfig = {
    'hiddenBlock': 'hidden-block',
    'fadeBlock': 'fade-block',
    'clickEvent': 'click',
    'overEvent': 'mouseover',
    'outEvent': 'mouseout',
    'noIsotope': false,
    'currentMenuItemClass': 'current-menu-item',
    'filterList': null
  };

  function createBlockCode(item)
  {
    var classes = [
      'filter-'+item.categoryId,
      'picture-block-item'
    ]

    var itemCode = '<li class="'+classes.join(' ')+'">';

    itemCode += '<a href="'+item.url+'" class="block-link"><img src="'+item.image+'" alt="" /></a>';

    itemCode += '<div class="block-content"><div class="block-detail">';
itemCode += '<p class="post-categories">'+item.category+'</p>';
itemCode += '<h3>'+item.title+'</h3>';
itemCode += '</div></div>';

    itemCode += '</li>';

    return itemCode;
  }

  function getPostsDatas(iO,cat)
  {
    iO.isotope('remove',jqAjax('li.picture-block-item'));
    
    jqAjax.ajax({
      url: homePosts.ajaxurl,
      data: {action: 'filter_posts', category: cat},
      success: function(response)
      {
        var pageCode = '';
        for(var i=0,count=response.length;i<count;i++)
        {
          pageCode += createBlockCode(response[i]);
        }

        if(pageCode != '')
        {
          var pageContent = jqAjax(pageCode);

          if(iO !== null)
          {
            iO.isotope('insert',pageContent,function()
            {
              if(window.displayPictureBlock)
              {
                displayPictureBlock();
              }
            });
          }
        }
      },
      dataType: 'json'
    });
  }

  var isotopeContainer = null;
  var isotopeObject = null;
  
  function isotopeWin()
  {
    var wW = jqAjax(this).width();

    if(wW <= 370)
    {
      _epiceAConfig.noIsotope = true;
    }
    else
    {
      _epiceAConfig.noIsotope = false;
    }

    if(isotopeContainer !== null && isotopeContainer !== undefined)
    {
      isotopeObject = isotopeContainer.data('isotope');
    }
    
    if(_epiceAConfig.noIsotope)
    {
      if(isotopeObject !== null && isotopeObject !== undefined)
      {
        isotopeObject.destroy();
      }
    }
    else
    {
      if(isotopeContainer !== null && isotopeContainer !== undefined && isotopeContainer.length > 0)
      {
        isotopeObject = isotopeContainer.isotope({
          itemSelector: '.picture-block-item',
          /*layoutMode: 'masonry',
          masonry: {
            columnWidth: '.post',
            gutter: 30
          },*/
          //layoutMode : 'fitRows',
          masonry: {
            columnWidth: 400
          },
          layoutMode : 'masonry',
          filter: '.picture-block-item'
        });
      }
    }
  }

  jqAjax(window).on('load',function()
  {
    
  });

  jqAjax(function()
  {
    isotopeContainer = jqAjax('.post-list');
    isotopeWin();

    _epiceAConfig.filterList = jqAjax('#filters-list li');
    
    jqAjax('body.home .filters-list li').on(_epiceAConfig.clickEvent,function(ev)
    {
      var currentItem = jqAjax(this);
      var prefix = '.filter-';

      if(_epiceAConfig.filterList !== null)
      {
        _epiceAConfig.filterList.removeClass(_epiceAConfig.currentMenuItemClass);
      }
      currentItem.addClass(_epiceAConfig.currentMenuItemClass);
      
      var filter = currentItem.data('filter');

      var selector = '';

      if(filter != '')
      {
        selector = prefix+filter;
      }
      
      if(selector != '')
      {
        //isotopeContainer.isotope({'filter': selector});
      }
      
      getPostsDatas(isotopeObject,filter);
      
      ev.preventDefault();
    });

    jqAjax('body.home header .home').css({'cursor': 'pointer'}).on(_epiceAConfig.clickEvent,function(ev)
    {
      //getPostsDatas(isotopeObject,'');
      //isotopeContainer.isotope({'filter': '.post'});

      window.location.reload();
    });
  });

  jqAjax(window).on('resize',isotopeWin);
}
