<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php bloginfo('language'); ?>" dir="ltr" vocab="http://schema.org/">

<head>

<meta charset="<?php bloginfo('charset'); ?>" />
<meta http-equiv="Content-Type" content="text/html; charset=<?php bloginfo('charset'); ?>" />
<meta name="viewport" content="initial-scale=1.0,user-scalable=no">
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>

<div id="contenu">
<?php get_template_part('parts/page-header'); ?>

<main id="main" role="main">

<div class="main-block">
  
<?php if(!is_front_page() && !is_category()): ?>
  <?php if(function_exists('yoast_breadcrumb')): ?>
  <?php yoast_breadcrumb('<p id="breadcrumbs" class="desktop-element">','</p>'); ?>
  <?php endif; ?>
<?php endif; ?>

