<?php
/**
 * The main template file
 *
 * @package Epice
 */

 
$defaultLocale = 'fr_FR';
$locale = get_locale();

if(empty($locale))
{
  $locale = $defaultLocale;
}

$args = array(
  'type'     => 'post',
  'child_of' => 0
);

$categories = array();

$locations = get_nav_menu_locations();
if(array_key_exists('catheader',$locations))
{
  $menu = wp_get_nav_menu_object($locations['catheader']);
  $catManu = wp_get_nav_menu_items($menu->term_id);

  foreach($catManu as $vCat)
  {
    if($vCat->object == 'category')
    {
      $categories[] = array(
        'id' => $vCat->object_id
      );
    }
  }
}

$results = array();

get_header();

?>

<ol reversed="reversed" class="post-list">

<?php foreach($categories as $kC => $vC):

  $argsPost = array(
    'cat' => $vC['id'],
    'post_type' => 'post',
    'posts_per_page' => 3
  );

  $loopPost = new WP_Query($argsPost);
  while($loopPost->have_posts()): $loopPost->the_post();
?>
<?php get_template_part('parts/list-block'); ?>
  <?php endwhile; wp_reset_query(); ?>
<?php endforeach; ?>

</ol>

<?php
get_footer();
