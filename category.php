<?php

get_header(); ?>


<?php if(have_posts()): ?>
<ol reversed="reversed" class="post-list">
<?php while(have_posts()): the_post();?>
<?php get_template_part('parts/list-block'); ?>
<?php endwhile; ?>
</ol>
<?php endif; ?>


<?php

get_footer();
