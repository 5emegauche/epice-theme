<?php

$classCode = '';
$class = get_post_class();

$pos = ($index % 2 == 0) ? 'even' : 'odd';

$class[] = $pos;

if(count($class) > 0)
{
  $classCode = ' class="'.implode(' ',$class).'"';
}

?><li<?php echo $classCode; ?>>
<figure class="history-picture"><img alt="<?php echo esc_attr(strip_tags(($post->post_title))); ?>" src="<?php echo esc_url(get_post_meta($post->ID,THEME_PREFIX.'HistoryPicture', true)); ?>"></figure>

<div class="history-content">
  <p class="history-date"><?php echo get_post_meta($post->ID,THEME_PREFIX.'HistoryDate', true); ?></p>
  <h3 class="history-title"><?php the_title(); ?></h3>
  <div class="history-description">
  <?php echo get_post_meta($post->ID,THEME_PREFIX.'HistoryDescription', true); ?>
  </div>
</div>
</li>
