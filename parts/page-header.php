<?php

$defMenuConf = array(
  'container' => 'nav'
);

$leftMenuConf = array_merge($defMenuConf,array('theme_location' => 'leftheader','container_class' => 'top-nav left-nav desktop-element'));
$rightMenuConf = array_merge($defMenuConf,array('theme_location' => 'rightheader','container_class' => 'top-nav right-nav desktop-element'));

//$catMenuConf = array('container' => 'nav','container_class' => 'filters-list','theme_location' => 'catheader');

$categories = array();

$locations = get_nav_menu_locations();
if(array_key_exists('catheader',$locations))
{
  $menu = wp_get_nav_menu_object($locations['catheader']);
  $catManu = wp_get_nav_menu_items($menu->term_id);

  //print_r($catManu);

  foreach($catManu as $vCat)
  {
    if($vCat->object == 'category')
    {
      $categories[] = array(
        'url' => $vCat->url,
        'title' => $vCat->title,
        'id' => $vCat->object_id
      );
    }
  }
}

$queried_object = get_queried_object();

$currentCatId = null;
if(isset($queried_object->cat_ID))
{
  $currentCatId = $queried_object->cat_ID;
}

?>
<header class="page-header">

<?php /*
<div class="newsletter">
<h6><?php _e('Newsletter'); ?></h6>
<form id="newsletter-form">
<p><input type="text" name="" value="" class="input" id="newsletter-input" /><input type="submit" name="" value="" id="newsletter-submit" /></p>
</form>
</div>
*/ ?>

<?php if(is_active_sidebar('header_right_1')): ?>
<div id="primary-sidebar" class="primary-sidebar widget-area newsletter-block" role="complementary">
<?php dynamic_sidebar('header_right_1'); ?>
</div>
<?php endif; ?>

<div class="site-info">
<?php if(is_front_page()): ?><h1 class="home image-text"><?php else: ?><a href="<?php echo home_url(); ?>" class="home image-text"><?php endif; ?>
<?php bloginfo('title'); ?>
<?php if(is_front_page()): ?></h1><?php else: ?></a><?php endif; ?>
<p class="sub"><?php //bloginfo('description'); ?><?php _e('Bess Nielsen & Jan Machenhauer'); ?></p>
</div>

<div class="header-bottom">
<?php wp_nav_menu($leftMenuConf); ?>

<?php //wp_nav_menu($catMenuConf); ?>

<?php if(count($categories) > 0): ?>
<nav class="filters-list desktop-element">
  <ul id="filters-list">
<?php foreach($categories as $cat): ?>
<?php

$classCode = '';
$classAr = array();

if($currentCatId == $cat['id'])
{
  $classAr[] = 'current-menu-item';
}

if(count($classAr) > 0)
{
  $classCode = ' class="'.implode(' ',$classAr).'"';
}

?>
<li data-filter="<?php echo $cat['id']; ?>"<?php echo $classCode; ?>><a href="<?php echo $cat['url']; ?>"><?php echo $cat['title']; ?></a></li>
<?php endforeach; ?>
  </ul>
</nav>
<?php endif; ?>

<?php wp_nav_menu($rightMenuConf); ?>
</div>
</header>

