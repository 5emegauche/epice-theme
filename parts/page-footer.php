<?php

$footerMenuConf = array(
  'theme_location' => 'footer',
  'container' => 'nav',
  'container_class' => 'bottom-nav'
);

?>

<footer class="page-footer">
<nav class="social-nav">
<h4><?php _e('Follow us on',THEME_PREFIX); ?></h4>
<ul id="social-nav-list">
<li><a href="" class="facebook image-text"><?php _e('Facebook',THEME_PREFIX); ?></a></li>
<li><a href="" class="twitter image-text"><?php _e('Twitter',THEME_PREFIX); ?></a></li>
<li><a href="" class="instagram image-text"><?php _e('Instagram',THEME_PREFIX); ?></a></li>
<li><a href="" class="pinterest image-text"><?php _e('Pinterest',THEME_PREFIX); ?></a></li>
<li><a href="" class="google-plus image-text"><?php _e('Google Plus',THEME_PREFIX); ?></a></li>
<li><a href="" class="youtube image-text"><?php _e('Youtube',THEME_PREFIX); ?></a></li>
</ul>
</nav>

<div class="bottom-footer">
  <div class="lang-switcher" id="lang-switcher">
  <?php if(function_exists("the_msls"))the_msls(); ?>
  </div>

<p class="copyright">&copy; épice 2015</p>

<?php wp_nav_menu($footerMenuConf); ?>

</div>
</footer>
