<?php

$blockCats = get_the_category();
$blockCat = $blockCats[0];

$supClass = array('filter-'.$blockCat->cat_ID,'picture-block-item');

$pClass = get_post_class($supClass);

?>
<li class="<?php echo implode(' ',$pClass); ?>">
<?php if(has_post_thumbnail()): ?>
<a href="<?php the_permalink(); ?>" class="block-link"><?php the_post_thumbnail('full'); ?></a>
<?php endif; ?>
<div class="block-content">
  <div class="block-detail">
<p class="post-categories"><?php echo $blockCat->cat_name; ?></p>
<h3><?php the_title(); ?></h3>
  </div>
</div>
</li>
